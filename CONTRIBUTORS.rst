===============================
Credits (in alphabetical order)
===============================

* Raphaël BLEUSE <cs@research.bleuse.net>
* Pascal BOUVRY <pascal.bouvry@uni.lu>
* Grégoire DANOY <gregoire.danoy@uni.lu>
* Emmanuel KIEFFER <emmanuel.kieffer@uni.lu>
* Jeff MEDER <jeffmeder2303@gmail.com>
* Maya Alexandra OLSZEWSKI <maya.olszewski@gmail.com>
* Martin ROSALIE <martin.rosalie@univ-perp.fr>
